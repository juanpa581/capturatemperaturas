﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntidadesTemperatura
{
    public class Usuarios
    {
        public int IdUsuario { get; set; }
        public string Nombre { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public int Nivel { get; set; }
        public string Contraseña { get; set; }
        public int FkIdPermiso { get; set; }

        public Usuarios(int idusuario, string nombre, string apellidopaterno, string apellidomaterno, int nivel, string contraseña, int fkidpermiso)
        {
            IdUsuario = idusuario;
            Nombre = nombre;
            ApellidoPaterno = apellidopaterno;
            ApellidoMaterno = apellidomaterno;
            Nivel = nivel;
            Contraseña = contraseña;
            FkIdPermiso = fkidpermiso;
        }
    }
}
