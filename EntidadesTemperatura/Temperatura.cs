﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntidadesTemperatura
{
    public class Temperatura
    {
        public int IdTemperatura { get; set; }
        public string Medir { get; set; }

        public Temperatura(int idtemperatura, string medir)
        {
            IdTemperatura = idtemperatura;
            Medir = medir;
        }
    }
}
