﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntidadesTemperatura
{
    public class Permisos
    {
        public int IdPermiso { get; set; }
        public string Permiso { get; set; }

        public Permisos(int idpermiso, string permiso)
        {
            IdPermiso = idpermiso;
            Permiso = permiso;
        }
    }
}
