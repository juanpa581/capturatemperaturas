﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using AccesoDatosTemperatura;
using EntidadesTemperatura;
using System.Windows.Forms;

namespace ManejadoresTemperatura
{
    public class ManejadorPermisos
    {
        ConexionBD cbd = new ConexionBD();
        public string Guardar(Permisos permisos)
        {
            return cbd.Comando(string.Format("insert into Permisos values(" +
                "null,'{0}')", permisos.Permiso));
        }
        public string Borrar(Permisos permisos)
        {
            return cbd.Comando(string.Format("delete from Permisos where IdPermiso={0}", permisos.IdPermiso));
        }
        public string Modificar(Permisos permisos)
        {
            return cbd.Comando(string.Format("update Permisos set Permiso = '{0}' where IdPermiso = {1}", permisos.Permiso, permisos.IdPermiso));
        }
        public DataSet Listado(string q, string tabla)
        {
            return cbd.Mostrar(q, tabla);
        }
        public void LlenaPuestos(ComboBox combo, string q, string tabla)
        {
            combo.DataSource = cbd.Mostrar(q, tabla).Tables[0];
            combo.DisplayMember = "rolusuarios";
        }
    }
}
