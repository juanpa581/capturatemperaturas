﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoDatosTemperatura;

namespace ManejadoresTemperatura
{
    public class ManejadorLogin
    {
        ConexionLogin cl = new ConexionLogin();
        public bool validarUsuarios(string usuario, string contraseña)
        {
            return cl.IniciarSesion(usuario, contraseña);
        }
    }
}
