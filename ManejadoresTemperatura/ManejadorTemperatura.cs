﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using AccesoDatosTemperatura;
using EntidadesTemperatura;
using System.Windows.Forms;

namespace ManejadoresTemperatura
{
    public class ManejadorTemperatura
    {
        ConexionBD cbd = new ConexionBD();
        public string Guardar(Temperatura temperatura)
        {
            return cbd.Comando(string.Format("insert into Temperatura values(" +
                "null,'{0}')", temperatura.Medir));
        }
        public string Borrar(Temperatura temperatura)
        {
            return cbd.Comando(string.Format("delete from Temperatura where IdTemperatura={0}", temperatura.IdTemperatura));
        }
        public string Modificar(Temperatura temperatura)
        {
            return cbd.Comando(string.Format("update Temperatura set Medir = '{0}' where IdTemperatura = {1}", temperatura.Medir, temperatura.IdTemperatura));
        }
        public DataSet Listado(string q, string tabla)
        {
            return cbd.Mostrar(q, tabla);
        }
        public void LlenaPuestos(ComboBox combo, string q, string tabla)
        {
            combo.DataSource = cbd.Mostrar(q, tabla).Tables[0];
            combo.DisplayMember = "rolusuarios";
        }
    }
}
