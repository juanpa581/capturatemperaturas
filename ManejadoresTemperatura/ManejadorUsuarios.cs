﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using AccesoDatosTemperatura;
using EntidadesTemperatura;
using System.Windows.Forms;

namespace ManejadoresTemperatura
{
   public class ManejadorUsuarios
    {
        ConexionBD cbd = new ConexionBD();
        public string Guardar(Usuarios usuarios)
        {
            return cbd.Comando(string.Format("insert into Usuarios values(" +
                "null,'{0}', '{1}', '{2}', {3}, md5('{4}'), {5})", usuarios.Nombre, usuarios.ApellidoPaterno, usuarios.ApellidoMaterno, usuarios.Nivel, usuarios.Contraseña, usuarios.FkIdPermiso));
        }
        public string Borrar(Usuarios usuarios)
        {
            return cbd.Comando(string.Format("delete from Usuarios where IdUsuario={0}", usuarios.IdUsuario));
        }
        public string Modificar(Usuarios usuarios)
        {
            return cbd.Comando(string.Format("update Usuarios set Nombre = '{0}', ApellidoPaterno = '{1}',  ApellidoMaterno = '{2}', Nivel = {3}, Contraseña = md5('{4}'), FkIdPermiso = {5}  where IdUsuario = {6}", usuarios.Nombre, usuarios.ApellidoPaterno, usuarios.ApellidoMaterno, usuarios.Nivel, usuarios.Contraseña, usuarios.FkIdPermiso, usuarios.IdUsuario));
        }
        public DataSet Listado(string q, string tabla)
        {
            return cbd.Mostrar(q, tabla);
        }
        public void LlenaPuestos(ComboBox combo, string q, string tabla)
        {
            combo.DataSource = cbd.Mostrar(q, tabla).Tables[0];
            combo.DisplayMember = "rolusuarios";
        }
    }
}
