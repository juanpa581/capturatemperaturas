﻿
namespace PresentacionTemperatura
{
    partial class frmMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMenu));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tsbPermisos = new System.Windows.Forms.ToolStripButton();
            this.tsbTemperatura = new System.Windows.Forms.ToolStripButton();
            this.tsbUsuarios = new System.Windows.Forms.ToolStripButton();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbPermisos,
            this.tsbTemperatura,
            this.tsbUsuarios});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1200, 71);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // tsbPermisos
            // 
            this.tsbPermisos.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbPermisos.Image = ((System.Drawing.Image)(resources.GetObject("tsbPermisos.Image")));
            this.tsbPermisos.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbPermisos.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbPermisos.Name = "tsbPermisos";
            this.tsbPermisos.Size = new System.Drawing.Size(68, 68);
            this.tsbPermisos.Text = "Permisos";
            this.tsbPermisos.Click += new System.EventHandler(this.tsbPermisos_Click);
            // 
            // tsbTemperatura
            // 
            this.tsbTemperatura.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbTemperatura.Image = ((System.Drawing.Image)(resources.GetObject("tsbTemperatura.Image")));
            this.tsbTemperatura.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbTemperatura.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbTemperatura.Name = "tsbTemperatura";
            this.tsbTemperatura.Size = new System.Drawing.Size(68, 68);
            this.tsbTemperatura.Text = "Temperatura";
            this.tsbTemperatura.Click += new System.EventHandler(this.tsbTemperatura_Click);
            // 
            // tsbUsuarios
            // 
            this.tsbUsuarios.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbUsuarios.Image = ((System.Drawing.Image)(resources.GetObject("tsbUsuarios.Image")));
            this.tsbUsuarios.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbUsuarios.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbUsuarios.Name = "tsbUsuarios";
            this.tsbUsuarios.Size = new System.Drawing.Size(68, 68);
            this.tsbUsuarios.Text = "Usuarios";
            this.tsbUsuarios.Click += new System.EventHandler(this.tsbUsuarios_Click);
            // 
            // frmMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1200, 623);
            this.Controls.Add(this.toolStrip1);
            this.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.IsMdiContainer = true;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmMenu";
            this.Text = "frmMenu";
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton tsbPermisos;
        private System.Windows.Forms.ToolStripButton tsbTemperatura;
        private System.Windows.Forms.ToolStripButton tsbUsuarios;
    }
}