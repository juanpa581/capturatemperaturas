﻿
namespace PresentacionTemperatura
{
    partial class frmPermisos
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblCapturaDePermisos = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnAgregar = new System.Windows.Forms.Button();
            this.txtBuscarPermisos = new System.Windows.Forms.TextBox();
            this.lblBuscarPermisos = new System.Windows.Forms.Label();
            this.btnRegresar = new System.Windows.Forms.Button();
            this.btnModificar = new System.Windows.Forms.Button();
            this.btnBorrar = new System.Windows.Forms.Button();
            this.dtgPermisos = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgPermisos)).BeginInit();
            this.SuspendLayout();
            // 
            // lblCapturaDePermisos
            // 
            this.lblCapturaDePermisos.AutoSize = true;
            this.lblCapturaDePermisos.BackColor = System.Drawing.Color.Transparent;
            this.lblCapturaDePermisos.Location = new System.Drawing.Point(476, 62);
            this.lblCapturaDePermisos.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lblCapturaDePermisos.Name = "lblCapturaDePermisos";
            this.lblCapturaDePermisos.Size = new System.Drawing.Size(157, 18);
            this.lblCapturaDePermisos.TabIndex = 72;
            this.lblCapturaDePermisos.Text = "Captura de Permisos";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pictureBox1.Location = new System.Drawing.Point(88, 22);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(8, 6, 8, 6);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1028, 97);
            this.pictureBox1.TabIndex = 71;
            this.pictureBox1.TabStop = false;
            // 
            // btnAgregar
            // 
            this.btnAgregar.Location = new System.Drawing.Point(990, 140);
            this.btnAgregar.Name = "btnAgregar";
            this.btnAgregar.Size = new System.Drawing.Size(75, 23);
            this.btnAgregar.TabIndex = 70;
            this.btnAgregar.Text = "+";
            this.btnAgregar.UseVisualStyleBackColor = true;
            this.btnAgregar.Click += new System.EventHandler(this.btnAgregar_Click);
            // 
            // txtBuscarPermisos
            // 
            this.txtBuscarPermisos.Location = new System.Drawing.Point(252, 142);
            this.txtBuscarPermisos.Margin = new System.Windows.Forms.Padding(4);
            this.txtBuscarPermisos.Name = "txtBuscarPermisos";
            this.txtBuscarPermisos.Size = new System.Drawing.Size(501, 26);
            this.txtBuscarPermisos.TabIndex = 69;
            this.txtBuscarPermisos.TextChanged += new System.EventHandler(this.txtBuscarPermisos_TextChanged);
            // 
            // lblBuscarPermisos
            // 
            this.lblBuscarPermisos.AutoSize = true;
            this.lblBuscarPermisos.Location = new System.Drawing.Point(85, 142);
            this.lblBuscarPermisos.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblBuscarPermisos.Name = "lblBuscarPermisos";
            this.lblBuscarPermisos.Size = new System.Drawing.Size(128, 18);
            this.lblBuscarPermisos.TabIndex = 68;
            this.lblBuscarPermisos.Text = "Buscar Permisos";
            // 
            // btnRegresar
            // 
            this.btnRegresar.Location = new System.Drawing.Point(953, 568);
            this.btnRegresar.Margin = new System.Windows.Forms.Padding(4);
            this.btnRegresar.Name = "btnRegresar";
            this.btnRegresar.Size = new System.Drawing.Size(112, 32);
            this.btnRegresar.TabIndex = 67;
            this.btnRegresar.Text = "Regresar";
            this.btnRegresar.UseVisualStyleBackColor = true;
            this.btnRegresar.Click += new System.EventHandler(this.btnRegresar_Click);
            // 
            // btnModificar
            // 
            this.btnModificar.Location = new System.Drawing.Point(566, 568);
            this.btnModificar.Margin = new System.Windows.Forms.Padding(4);
            this.btnModificar.Name = "btnModificar";
            this.btnModificar.Size = new System.Drawing.Size(112, 32);
            this.btnModificar.TabIndex = 66;
            this.btnModificar.Text = "Modificar";
            this.btnModificar.UseVisualStyleBackColor = true;
            this.btnModificar.Click += new System.EventHandler(this.btnModificar_Click);
            // 
            // btnBorrar
            // 
            this.btnBorrar.Location = new System.Drawing.Point(88, 568);
            this.btnBorrar.Margin = new System.Windows.Forms.Padding(4);
            this.btnBorrar.Name = "btnBorrar";
            this.btnBorrar.Size = new System.Drawing.Size(112, 32);
            this.btnBorrar.TabIndex = 65;
            this.btnBorrar.Text = "Borrar";
            this.btnBorrar.UseVisualStyleBackColor = true;
            this.btnBorrar.Click += new System.EventHandler(this.btnBorrar_Click);
            // 
            // dtgPermisos
            // 
            this.dtgPermisos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgPermisos.Location = new System.Drawing.Point(88, 193);
            this.dtgPermisos.Margin = new System.Windows.Forms.Padding(4);
            this.dtgPermisos.Name = "dtgPermisos";
            this.dtgPermisos.Size = new System.Drawing.Size(1028, 316);
            this.dtgPermisos.TabIndex = 64;
            this.dtgPermisos.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgPermisos_CellClick);
            // 
            // frmPermisos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1200, 623);
            this.Controls.Add(this.lblCapturaDePermisos);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnAgregar);
            this.Controls.Add(this.txtBuscarPermisos);
            this.Controls.Add(this.lblBuscarPermisos);
            this.Controls.Add(this.btnRegresar);
            this.Controls.Add(this.btnModificar);
            this.Controls.Add(this.btnBorrar);
            this.Controls.Add(this.dtgPermisos);
            this.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmPermisos";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.frmPermisos_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgPermisos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblCapturaDePermisos;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnAgregar;
        private System.Windows.Forms.TextBox txtBuscarPermisos;
        private System.Windows.Forms.Label lblBuscarPermisos;
        private System.Windows.Forms.Button btnRegresar;
        private System.Windows.Forms.Button btnModificar;
        private System.Windows.Forms.Button btnBorrar;
        private System.Windows.Forms.DataGridView dtgPermisos;
    }
}

