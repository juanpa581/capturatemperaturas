﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ManejadoresTemperatura;

namespace PresentacionTemperatura
{
    public partial class frmInicioSesion : Form
    {
        frmMenu m = new frmMenu();
        ManejadorLogin ml;
        bool c = false;
        public frmInicioSesion()
        {
            InitializeComponent();
            ml = new ManejadorLogin();
            //SqlConnection con = new SqlConnection("localhost", "root", "", "Temperatura");
        }

        private void btnIniciarSesion_Click(object sender, EventArgs e)
        {
            if (ml.validarUsuarios(txtUsuario.Text, txtContraseña.Text))
            {
                m.Show();
            }
            else
            {
                MessageBox.Show("El nombre de usuario o la contraseña no coinciden con un usuario registrado en la base de datos",
                    "¡ERROR DE INICIO DE SESIÓN!");
            }
        }

        private void cbOcultar_CheckedChanged(object sender, EventArgs e)
        {
            if (c == false)
            {
                //btnContraseña.ImageLocation = "Contraseña/mostrar.png";
                txtContraseña.PasswordChar = '\0';
                c = true;
            }
            else
            {
                //btnContraseña.ImageLocation = "Contraseña/ocultar.png";
                txtContraseña.PasswordChar = '*';
                c = false;
            }
        }
    }
}
