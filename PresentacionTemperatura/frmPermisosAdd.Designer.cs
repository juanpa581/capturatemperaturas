﻿
namespace PresentacionTemperatura
{
    partial class frmPermisosAdd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtPermiso = new System.Windows.Forms.TextBox();
            this.txtIdPermiso = new System.Windows.Forms.TextBox();
            this.lblPermiso = new System.Windows.Forms.Label();
            this.lblIdPermiso = new System.Windows.Forms.Label();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtPermiso
            // 
            this.txtPermiso.Location = new System.Drawing.Point(122, 86);
            this.txtPermiso.Name = "txtPermiso";
            this.txtPermiso.Size = new System.Drawing.Size(213, 26);
            this.txtPermiso.TabIndex = 13;
            // 
            // txtIdPermiso
            // 
            this.txtIdPermiso.Location = new System.Drawing.Point(122, 38);
            this.txtIdPermiso.Name = "txtIdPermiso";
            this.txtIdPermiso.Size = new System.Drawing.Size(213, 26);
            this.txtIdPermiso.TabIndex = 12;
            // 
            // lblPermiso
            // 
            this.lblPermiso.AutoSize = true;
            this.lblPermiso.Location = new System.Drawing.Point(19, 89);
            this.lblPermiso.Name = "lblPermiso";
            this.lblPermiso.Size = new System.Drawing.Size(67, 18);
            this.lblPermiso.TabIndex = 11;
            this.lblPermiso.Text = "Permiso";
            // 
            // lblIdPermiso
            // 
            this.lblIdPermiso.AutoSize = true;
            this.lblIdPermiso.Location = new System.Drawing.Point(19, 38);
            this.lblIdPermiso.Name = "lblIdPermiso";
            this.lblIdPermiso.Size = new System.Drawing.Size(83, 18);
            this.lblIdPermiso.TabIndex = 10;
            this.lblIdPermiso.Text = "Id Permiso";
            // 
            // btnGuardar
            // 
            this.btnGuardar.Location = new System.Drawing.Point(30, 189);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(113, 34);
            this.btnGuardar.TabIndex = 14;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(231, 189);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(120, 34);
            this.btnCancelar.TabIndex = 15;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // frmPermisosAdd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.ClientSize = new System.Drawing.Size(397, 258);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.txtPermiso);
            this.Controls.Add(this.txtIdPermiso);
            this.Controls.Add(this.lblPermiso);
            this.Controls.Add(this.lblIdPermiso);
            this.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmPermisosAdd";
            this.Text = "frmPermisosAdd";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox txtPermiso;
        private System.Windows.Forms.TextBox txtIdPermiso;
        private System.Windows.Forms.Label lblPermiso;
        private System.Windows.Forms.Label lblIdPermiso;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.Button btnCancelar;
    }
}