﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ManejadoresTemperatura;
using EntidadesTemperatura;

namespace PresentacionTemperatura
{
    public partial class frmUsuarios : Form
    {
        ManejadorUsuarios mu;
        Usuarios u = new Usuarios(0,"","","",0,"",0);
        int fila = 0;
        string r = "";
        public frmUsuarios()
        {
            InitializeComponent();
            mu = new ManejadorUsuarios();
        }
        void Actualizar()
        {
            dtgUsuarios.DataSource = mu.Listado(string.Format("" +
                "Select * from Usuarios where Nombre like '%{0}%'", 
                txtBuscarNombreUsuario.Text), "Usuarios").Tables[0];
            dtgUsuarios.AutoResizeColumns();
            dtgUsuarios.Columns[0].ReadOnly = true;
        }

        private void frmUsuarios_Load(object sender, EventArgs e)
        {
            Actualizar();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            frmUsuariosAdd fua = new frmUsuariosAdd();
            fua.ShowDialog();
            Actualizar();
        }

        private void txtBuscarNombreUsuario_TextChanged(object sender, EventArgs e)
        {
            Actualizar();
        }

        private void btnRegresar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void dtgUsuarios_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            fila = e.RowIndex;
            u.IdUsuario = int.Parse(dtgUsuarios.Rows[fila].Cells[0].Value.ToString());
            u.Nombre = dtgUsuarios.Rows[fila].Cells[1].Value.ToString();
            u.ApellidoPaterno = dtgUsuarios.Rows[fila].Cells[2].Value.ToString();
            u.ApellidoMaterno = dtgUsuarios.Rows[fila].Cells[3].Value.ToString();
            u.Nivel = int.Parse(dtgUsuarios.Rows[fila].Cells[4].Value.ToString());
            u.Contraseña = dtgUsuarios.Rows[fila].Cells[5].Value.ToString();
            u.FkIdPermiso = int.Parse(dtgUsuarios.Rows[fila].Cells[6].Value.ToString());
        }

        private void btnBorrar_Click(object sender, EventArgs e)
        {
            DialogResult rs = MessageBox.Show("Atención está seguro de borrar l?" + u.Nombre,
                "|Atención|", MessageBoxButtons.YesNo);
            if (rs == DialogResult.Yes)
            {
                r = mu.Borrar(u);
                Actualizar();
            }
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            frmUsuariosAdd fua = new frmUsuariosAdd(u);
            fua.ShowDialog();
            Actualizar();
        }
    }
}
