﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ManejadoresTemperatura;
using EntidadesTemperatura;

namespace PresentacionTemperatura
{
    public partial class frmPermisosAdd : Form
    {
        ManejadorPermisos mp = new ManejadorPermisos();
        Permisos p;
        int id = 0;
        public frmPermisosAdd()
        {
            InitializeComponent();
        }
        public frmPermisosAdd(Permisos permisos)
        {
            InitializeComponent();
            id = permisos.IdPermiso;
            txtPermiso.Text = permisos.Permiso;
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (id > 0)
            {
                p = new Permisos(id, txtPermiso.Text);
                string r = mp.Modificar(p);
                Close();
            }
            else
                try
                {
                    string resultado = mp.Guardar(new Permisos(0, txtPermiso.Text));
                    Close();
                }
                catch (Exception)
                {
                    MessageBox.Show("Error de dato");
                    txtPermiso.Focus();
                }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
