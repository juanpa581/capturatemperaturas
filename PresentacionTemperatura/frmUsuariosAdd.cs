﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ManejadoresTemperatura;
using EntidadesTemperatura;

namespace PresentacionTemperatura
{
    public partial class frmUsuariosAdd : Form
    {
        ManejadorUsuarios mu = new ManejadorUsuarios();
        Usuarios u;
        int id = 0;
        public frmUsuariosAdd()
        {
            InitializeComponent();
        }
        public frmUsuariosAdd(Usuarios usuarios)
        {
            InitializeComponent();
            id = usuarios.IdUsuario;
            txtNombre.Text = usuarios.Nombre;
            txtApellidoPaterno.Text = usuarios.ApellidoPaterno;
            txtApellidoMaterno.Text = usuarios.ApellidoMaterno;
            txtNivel.Text = usuarios.Nivel.ToString();
            txtContraseña.Text = usuarios.Contraseña;
            txtFkIdPermiso.Text = usuarios.FkIdPermiso.ToString();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (id > 0)
            {
                u = new Usuarios(id, txtNombre.Text, txtApellidoPaterno.Text, txtApellidoMaterno.Text, 
                    int.Parse(txtNivel.Text), txtContraseña.Text, int.Parse(txtFkIdPermiso.Text));
                string r = mu.Modificar(u);
                Close();
            }
            else
                try
                {
                    string resultado = mu.Guardar(new Usuarios(0, txtNombre.Text, txtApellidoPaterno.Text, 
                        txtApellidoMaterno.Text, int.Parse(txtNivel.Text), txtContraseña.Text, int.Parse(txtFkIdPermiso.Text)));
                    Close();
                }
                catch (Exception)
                {
                    MessageBox.Show("Error de dato");
                    txtNombre.Focus();
                }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
