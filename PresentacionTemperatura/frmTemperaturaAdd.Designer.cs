﻿
namespace PresentacionTemperatura
{
    partial class frmTemperaturaAdd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.txtMedir = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.lblMedir = new System.Windows.Forms.Label();
            this.lblIdTemperatura = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(259, 197);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(120, 34);
            this.btnCancelar.TabIndex = 13;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnGuardar
            // 
            this.btnGuardar.Location = new System.Drawing.Point(58, 197);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(113, 34);
            this.btnGuardar.TabIndex = 12;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // txtMedir
            // 
            this.txtMedir.Location = new System.Drawing.Point(171, 114);
            this.txtMedir.Name = "txtMedir";
            this.txtMedir.Size = new System.Drawing.Size(223, 26);
            this.txtMedir.TabIndex = 11;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(171, 35);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(223, 26);
            this.textBox1.TabIndex = 10;
            // 
            // lblMedir
            // 
            this.lblMedir.AutoSize = true;
            this.lblMedir.Location = new System.Drawing.Point(53, 117);
            this.lblMedir.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMedir.Name = "lblMedir";
            this.lblMedir.Size = new System.Drawing.Size(48, 18);
            this.lblMedir.TabIndex = 9;
            this.lblMedir.Text = "Medir";
            // 
            // lblIdTemperatura
            // 
            this.lblIdTemperatura.AutoSize = true;
            this.lblIdTemperatura.Location = new System.Drawing.Point(53, 38);
            this.lblIdTemperatura.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblIdTemperatura.Name = "lblIdTemperatura";
            this.lblIdTemperatura.Size = new System.Drawing.Size(111, 18);
            this.lblIdTemperatura.TabIndex = 8;
            this.lblIdTemperatura.Text = "Id Temperatura";
            // 
            // frmTemperaturaAdd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.ClientSize = new System.Drawing.Size(439, 263);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.txtMedir);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.lblMedir);
            this.Controls.Add(this.lblIdTemperatura);
            this.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "frmTemperaturaAdd";
            this.Text = "frmTemperaturaAdd";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.TextBox txtMedir;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label lblMedir;
        private System.Windows.Forms.Label lblIdTemperatura;
    }
}