﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ManejadoresTemperatura;
using EntidadesTemperatura;

namespace PresentacionTemperatura
{
    public partial class frmTemperatura : Form
    {
        ManejadorTemperatura mt;
        Temperatura t = new Temperatura(0,"");
        int fila = 0;
        string r = "";
        public frmTemperatura()
        {
            InitializeComponent();
            mt = new ManejadorTemperatura();
        }
        void Actualizar()
        {
            dtgTemperatura.DataSource = mt.Listado(string.Format("" +
                "Select * from Temperatura where IdTemperatura like '%{0}%'", 
                txtBuscarTemperatura.Text), "Temperatura").Tables[0];
            dtgTemperatura.AutoResizeColumns();
            dtgTemperatura.Columns[0].ReadOnly = true;
        }

        private void frmTemperatura_Load(object sender, EventArgs e)
        {
            Actualizar();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            frmTemperaturaAdd fta = new frmTemperaturaAdd();
            fta.ShowDialog();
            Actualizar();
        }

        private void txtBuscarTemperatura_TextChanged(object sender, EventArgs e)
        {
            Actualizar();
        }

        private void btnRegresar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void dtgTemperatura_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            fila = e.RowIndex;
            t.IdTemperatura = int.Parse(dtgTemperatura.Rows[fila].Cells[0].Value.ToString());
            t.Medir = dtgTemperatura.Rows[fila].Cells[1].Value.ToString();
        }

        private void btnBorrar_Click(object sender, EventArgs e)
        {
            DialogResult rs = MessageBox.Show("Atención está seguro de borrar l?" + t.IdTemperatura,
                "|Atención|", MessageBoxButtons.YesNo);
            if (rs == DialogResult.Yes)
            {
                r = mt.Borrar(t);
                Actualizar();
            }
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            frmTemperaturaAdd fta = new frmTemperaturaAdd(t);
            fta.ShowDialog();
            Actualizar();
        }
    }
}
