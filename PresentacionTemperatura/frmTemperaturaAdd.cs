﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ManejadoresTemperatura;
using EntidadesTemperatura;

namespace PresentacionTemperatura
{
    public partial class frmTemperaturaAdd : Form
    {
        ManejadorTemperatura mt = new ManejadorTemperatura();
        Temperatura t;
        int id = 0;
        public frmTemperaturaAdd()
        {
            InitializeComponent();
        }
        public frmTemperaturaAdd(Temperatura temperatura)
        {
            InitializeComponent();
            id = temperatura.IdTemperatura;
            txtMedir.Text = temperatura.Medir;
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (id > 0)
            {
                t = new Temperatura(id, txtMedir.Text);
                string r = mt.Modificar(t);
                Close();
            }
            else
                try
                {
                    string resultado = mt.Guardar(new Temperatura(0, txtMedir.Text));
                    Close();
                }
                catch (Exception)
                {
                    MessageBox.Show("Error de dato");
                    txtMedir.Focus();
                }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
