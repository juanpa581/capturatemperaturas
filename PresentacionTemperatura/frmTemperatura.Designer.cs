﻿
namespace PresentacionTemperatura
{
    partial class frmTemperatura
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblCapturaDeTemperatura = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnAgregar = new System.Windows.Forms.Button();
            this.txtBuscarTemperatura = new System.Windows.Forms.TextBox();
            this.lblBuscarTemperatura = new System.Windows.Forms.Label();
            this.btnRegresar = new System.Windows.Forms.Button();
            this.btnModificar = new System.Windows.Forms.Button();
            this.btnBorrar = new System.Windows.Forms.Button();
            this.dtgTemperatura = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgTemperatura)).BeginInit();
            this.SuspendLayout();
            // 
            // lblCapturaDeTemperatura
            // 
            this.lblCapturaDeTemperatura.AutoSize = true;
            this.lblCapturaDeTemperatura.BackColor = System.Drawing.Color.Transparent;
            this.lblCapturaDeTemperatura.Location = new System.Drawing.Point(476, 62);
            this.lblCapturaDeTemperatura.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lblCapturaDeTemperatura.Name = "lblCapturaDeTemperatura";
            this.lblCapturaDeTemperatura.Size = new System.Drawing.Size(174, 18);
            this.lblCapturaDeTemperatura.TabIndex = 63;
            this.lblCapturaDeTemperatura.Text = "Captura de temperatura";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pictureBox1.Location = new System.Drawing.Point(88, 22);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(8, 6, 8, 6);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1028, 97);
            this.pictureBox1.TabIndex = 62;
            this.pictureBox1.TabStop = false;
            // 
            // btnAgregar
            // 
            this.btnAgregar.Location = new System.Drawing.Point(990, 140);
            this.btnAgregar.Name = "btnAgregar";
            this.btnAgregar.Size = new System.Drawing.Size(75, 23);
            this.btnAgregar.TabIndex = 61;
            this.btnAgregar.Text = "+";
            this.btnAgregar.UseVisualStyleBackColor = true;
            this.btnAgregar.Click += new System.EventHandler(this.btnAgregar_Click);
            // 
            // txtBuscarTemperatura
            // 
            this.txtBuscarTemperatura.Location = new System.Drawing.Point(252, 142);
            this.txtBuscarTemperatura.Margin = new System.Windows.Forms.Padding(4);
            this.txtBuscarTemperatura.Name = "txtBuscarTemperatura";
            this.txtBuscarTemperatura.Size = new System.Drawing.Size(501, 26);
            this.txtBuscarTemperatura.TabIndex = 60;
            this.txtBuscarTemperatura.TextChanged += new System.EventHandler(this.txtBuscarTemperatura_TextChanged);
            // 
            // lblBuscarTemperatura
            // 
            this.lblBuscarTemperatura.AutoSize = true;
            this.lblBuscarTemperatura.Location = new System.Drawing.Point(85, 142);
            this.lblBuscarTemperatura.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblBuscarTemperatura.Name = "lblBuscarTemperatura";
            this.lblBuscarTemperatura.Size = new System.Drawing.Size(148, 18);
            this.lblBuscarTemperatura.TabIndex = 59;
            this.lblBuscarTemperatura.Text = "Buscar Temperatura";
            // 
            // btnRegresar
            // 
            this.btnRegresar.Location = new System.Drawing.Point(953, 568);
            this.btnRegresar.Margin = new System.Windows.Forms.Padding(4);
            this.btnRegresar.Name = "btnRegresar";
            this.btnRegresar.Size = new System.Drawing.Size(112, 32);
            this.btnRegresar.TabIndex = 58;
            this.btnRegresar.Text = "Regresar";
            this.btnRegresar.UseVisualStyleBackColor = true;
            this.btnRegresar.Click += new System.EventHandler(this.btnRegresar_Click);
            // 
            // btnModificar
            // 
            this.btnModificar.Location = new System.Drawing.Point(566, 568);
            this.btnModificar.Margin = new System.Windows.Forms.Padding(4);
            this.btnModificar.Name = "btnModificar";
            this.btnModificar.Size = new System.Drawing.Size(112, 32);
            this.btnModificar.TabIndex = 57;
            this.btnModificar.Text = "Modificar";
            this.btnModificar.UseVisualStyleBackColor = true;
            this.btnModificar.Click += new System.EventHandler(this.btnModificar_Click);
            // 
            // btnBorrar
            // 
            this.btnBorrar.Location = new System.Drawing.Point(88, 568);
            this.btnBorrar.Margin = new System.Windows.Forms.Padding(4);
            this.btnBorrar.Name = "btnBorrar";
            this.btnBorrar.Size = new System.Drawing.Size(112, 32);
            this.btnBorrar.TabIndex = 56;
            this.btnBorrar.Text = "Borrar";
            this.btnBorrar.UseVisualStyleBackColor = true;
            this.btnBorrar.Click += new System.EventHandler(this.btnBorrar_Click);
            // 
            // dtgTemperatura
            // 
            this.dtgTemperatura.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgTemperatura.Location = new System.Drawing.Point(88, 193);
            this.dtgTemperatura.Margin = new System.Windows.Forms.Padding(4);
            this.dtgTemperatura.Name = "dtgTemperatura";
            this.dtgTemperatura.Size = new System.Drawing.Size(1028, 316);
            this.dtgTemperatura.TabIndex = 55;
            this.dtgTemperatura.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgTemperatura_CellClick);
            // 
            // frmTemperatura
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1200, 623);
            this.Controls.Add(this.lblCapturaDeTemperatura);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnAgregar);
            this.Controls.Add(this.txtBuscarTemperatura);
            this.Controls.Add(this.lblBuscarTemperatura);
            this.Controls.Add(this.btnRegresar);
            this.Controls.Add(this.btnModificar);
            this.Controls.Add(this.btnBorrar);
            this.Controls.Add(this.dtgTemperatura);
            this.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "frmTemperatura";
            this.Text = "frmTemperatura";
            this.Load += new System.EventHandler(this.frmTemperatura_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgTemperatura)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblCapturaDeTemperatura;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnAgregar;
        private System.Windows.Forms.TextBox txtBuscarTemperatura;
        private System.Windows.Forms.Label lblBuscarTemperatura;
        private System.Windows.Forms.Button btnRegresar;
        private System.Windows.Forms.Button btnModificar;
        private System.Windows.Forms.Button btnBorrar;
        private System.Windows.Forms.DataGridView dtgTemperatura;
    }
}