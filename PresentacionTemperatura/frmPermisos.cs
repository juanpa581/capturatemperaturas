﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ManejadoresTemperatura;
using EntidadesTemperatura;

namespace PresentacionTemperatura
{
    public partial class frmPermisos : Form
    {
        ManejadorPermisos mp;
        Permisos p = new Permisos(0,"");
        int fila = 0;
        string r = "";
        public frmPermisos()
        {
            InitializeComponent();
            mp = new ManejadorPermisos();
        }
        void Actualizar()
        {
            dtgPermisos.DataSource = mp.Listado(string.Format("" +
                "Select * from Permisos where Permiso like '%{0}%'", 
                txtBuscarPermisos.Text), "Permisos").Tables[0];
            dtgPermisos.AutoResizeColumns();
            dtgPermisos.Columns[0].ReadOnly = true;
        }

        private void frmPermisos_Load(object sender, EventArgs e)
        {
            Actualizar();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            frmPermisosAdd fpa = new frmPermisosAdd();
            fpa.ShowDialog();
            Actualizar();
        }

        private void txtBuscarPermisos_TextChanged(object sender, EventArgs e)
        {
            Actualizar();
        }

        private void btnRegresar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void dtgPermisos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            fila = e.RowIndex;
            p.IdPermiso = int.Parse(dtgPermisos.Rows[fila].Cells[0].Value.ToString());
            p.Permiso = dtgPermisos.Rows[fila].Cells[1].Value.ToString();
        }

        private void btnBorrar_Click(object sender, EventArgs e)
        {
            DialogResult rs = MessageBox.Show("Atención está seguro de borrar l?" + p.Permiso, "|Atención|", MessageBoxButtons.YesNo);
            if (rs == DialogResult.Yes)
            {
                r = mp.Borrar(p);
                Actualizar();
            }
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            frmPermisosAdd fpa = new frmPermisosAdd(p);
            fpa.ShowDialog();
            Actualizar();
        }
    }
}
