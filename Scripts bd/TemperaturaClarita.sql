drop database if exists Temperatura;
create database Temperatura;
use Temperatura;

create table Permisos(
IdPermiso int primary key auto_increment,
Permiso varchar(100));

create table Temperatura(
IdTemperatura int primary key auto_increment,
Medir varchar(100));

create table Usuarios(
IdUsuario int primary key auto_increment,
Nombre varchar(100),
ApellidoPaterno varchar(100),
ApellidoMaterno varchar(100),
Nivel int,
Contraseņa varchar(100),
FkIdPermiso int,
foreign key(FkIdPermiso) references Permisos(IdPermiso));

describe Permisos;
describe Temperatura;
describe Usuarios;