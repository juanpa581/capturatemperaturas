﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bases;
using System.Data;

namespace AccesoDatosTemperatura
{
    public class ConexionBD
    {
        Conectar c = new Conectar("localhost", "root", "", "Temperatura");

        public string Comando(string q)
        {
            return c.Comando(q);
        }

        public DataSet Mostrar(string q, string tabla)
        {
            return c.Consultar(q, tabla);
        }
    }
}
